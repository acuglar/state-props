const Lock = (props) => {
  return (
    <div>
      {!props.locked ? (
        <button onClick={props.travar}>Travar</button>
      ) : (
        <button onClick={props.travar}>Destravar</button>
      )}
    </div>
  );
};

export default Lock;
