const Switch = (props) => {
  return (
    <div>
      {props.showLogo ? (
        <button onClick={props.trocar} disabled={props.locked}>
          Mostrar
        </button>
      ) : (
        <button onClick={props.trocar} disabled={props.locked}>
          Esconder
        </button>
      )}
    </div>
  );
};

export default Switch;
