import { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Switch from './components/Switch';
import Lock from './components/Lock';

const App = () => {
  const [showLogo, setshowLogo] = useState(false);
  const [locked, setLocker] = useState(false);

  const handleShowLogo = () => {
    setshowLogo(!showLogo)
  }

  const handleLocked = () => {
    setLocker(!locked)
  }

  return (
    <div className="App">
      <header className="App-header">
        <img hidden={showLogo} src={logo} className="App-logo" alt="logo" />
        <Switch locked={locked} showLogo={showLogo} trocar={handleShowLogo} />
        <Lock locked={locked} travar={handleLocked} />
      </header>
    </div>
  );
}

export default App;
